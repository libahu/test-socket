const express = require('express');
const http = require('http');
const { Server } = require('socket.io')
const { createAdapter } = require('@socket.io/redis-adapter');
const { createClient } = require("redis");

const app = express();

const server = http.createServer(app);

server.listen(5000, () => {
	console.log(`🚀 Server in running on port ${5000}`)
})
const io = new Server(server, {
	allowEIO3: true,
	pingTimeout: 30000,
	pingInterval: 5000,
	allowUpgrades: true,
	cors: {
		origin: '*',
		methods: ['GET', 'POST', 'PUT'],
		credentials: true,
	},
});

const pubClient = createClient({ url: "redis://localhost:6379" });
const subClient = pubClient.duplicate();

io.adapter(createAdapter(pubClient, subClient));

const chat = io.of('/chat-box');

chat.on('connection', (socket) => {
	socket.on('join_room', async (data_response) => {
		console.log(data_response);
		socket.join(data_response);
	});
})