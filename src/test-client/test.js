const { io } = require('socket.io-client');

const socket = io('http://localhost:5000/chat-box', {
	withCredentials: true,
	transports: ['websocket']
});

socket.on('connect', () => {
	console.log(socket.id);
	socket.emit("join_room", socket.id)
	socket.on('connect-server', data => {
		console.log(data);
	})
});

socket.on('disconnect', (disconnect) => {
	console.log(socket.id);
	console.log(disconnect);
});